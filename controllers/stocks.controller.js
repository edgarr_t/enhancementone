const Stock = require('../models/stock.model');
const mongoose = require('mongoose');


function industryMatch(string){
  var result = Stock.find(
  {
    "Industry" : string
  }  ).limit(5)
  return result 
}

async function get_stock(ticker){
    const data = await Stock.find({Ticker:ticker})
    return data
}


//Simple version, without validation or sanitation
exports.test = function (req, res) {
    res.send('Greetings from the Stock controller!');
};

exports.stock_create = function (req, res) {    
    let stock = new Stock(req.body);
    stock.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('Stock Created successfully')
    })
};


exports.stock_delete = function (req, res) {    
    Stock.findOneAndDelete({Ticker: req.params.id}, function (err, stock) {
        if (err) return next(err);
        res.send("Stock deleted");
    })
};

exports.stock_update = function (req, res) {        
    Stock.findOneAndUpdate({Ticker: req.params.id}, {$set: req.body}, function (err, product) {
        if (err) return next(err);
        res.send('Stock updated.');
    });    
};


exports.stock_details_by_id = function (req, res) {
    Stock.findById(req.params.id, function (err, stock) {
        if (err) return next(err);
        res.send(stock);
    })
};

exports.stock_details = function (req, res) {
    Stock.find({ Ticker: req.params.id}, function (err, stock) {
        if (err) return next(err);
        res.send(stock);
    })
};


exports.stock_report = function (req, res) {    
    var stocksToFind = req.body.stocks
    var stocks = []
    for (i = 0 ; i < (stocksToFind.length + 1); i++){

        get_stock(stocksToFind[i]).then(function(data){
                console.log(data)
                stocks.push(data)
                console.log(i + "i " + stocks)
            
        })
        
    }
    
    res.send(stocks)
    

};



exports.stock_portfolio = function (req, res) {
    Stock.find({$text: {$search:req.params.company}}).exec(function (err, doc){
        console.log(req.params.company)
        console.log(doc)
        var list = []
        for (i = 0 ; i < doc.length ; i++){
            list.push(doc[i])
        }
        res.send(list)
        
    });
};

exports.industry_details = function (req, res) {
    Stock.find({"Industry" : req.params.industry }).limit(5).exec(function (err, doc) {        
        var list = []
        for (i = 0 ; i <  doc.length; i++){
            list.push(doc[i])
        }
        res.send(list)
    });

};
