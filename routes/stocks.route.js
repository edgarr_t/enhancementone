const express = require('express');
const router = express.Router();
const stock_controller = require('../controllers/stocks.controller');

router.get('/test', stock_controller.test);
router.post('/createStock', stock_controller.stock_create);
router.get('/id/:id', stock_controller.stock_details_by_id);
router.get('/ticker/:id', stock_controller.stock_details);
router.get('/portfolio/:company', stock_controller.stock_portfolio);
router.get('/industryReport/:industry', stock_controller.industry_details);
router.delete('/ticker/:id/delete', stock_controller.stock_delete);
router.put('/ticker/:id/update', stock_controller.stock_update);
router.post('/stockReports', stock_controller.stock_report);




module.exports = router;